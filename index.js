// import express from 'express';
// import path from 'path';

const express = require('express');
const path = require('path');
// var config = require('config');
const app = express();
const port = 3434;

app.use(express.static('public'));

app.get('/', (req, res) => {
  return res.sendFile(path.join(__dirname + '/index.html'))
});

app.listen(port, () => console.log(`Paytm Login app listening on port ${port}!`))