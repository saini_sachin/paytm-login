var loginBox = document.getElementsByClassName("login-container")[0],
  submitBtn = document.getElementById("submitBtn"),
  resetBtn = document.getElementById("resetBtn"),
  usernameInput = document.getElementById("usernameInput"),
  passwordInput = document.getElementById("passwordInput"),
  ssoTokenInput = document.getElementById("show_sso_token"),
  walletTokenInput = document.getElementById("show_wallet_token"),
  otpWrapper = document.getElementsByClassName("otp-wrapper")[0],
  submitOtp = document.getElementById("submitOtp"),
  otpInput = document.getElementById("otpInput"),
  tokenInput = document.getElementById('tokenInput'),
  closeOtp = document.getElementById('closeOtp'),
  copyToken = document.getElementById('copyToken'),
  loaderComp = document.getElementById('loaderComp'),
  env = 'staging',
  // env = 'prod',
  CONFIG = {
    HEROKU_URL: "https://cors-anywhere.herokuapp.com/",
    SSO_TOKEN: "",
    WALLET_TOKEN: "",
    RESP_STATE: "",
    AUTH_STATE_URL:
      "oauth2/authorize?deviceIdentifier=LENOVO-LenovoA6000-868627023633499&deviceManufacturer=LENOVO&deviceName=Lenovo_A6000&client=androidapp&version=7.3.4&playStore=false&lat=0.0&long=0.0&language=en&networkType=WIFI&imei=868627023633499&osVersion=5.0.2&sim1=89918720400054456304&locale=en-IN&child_site_id=1&site_id=1",
    AUTH_ACCESS_TOKEN_URL:
      "oauth2/token?deviceIdentifier=LENOVO-LenovoA6000-868627023633499&deviceManufacturer=LENOVO&deviceName=Lenovo_A6000&client=androidapp&version=7.1.0&playStore=false&lat=0.0&long=0.0&language=en&networkType=WIFI&imei=868627023633499&osVersion=5.0.2&sim1=89918720400054456304",
    AUTH_SSO_TOKEN_URL_HOST: "oauth2/usertokens/",
    AUTH_SSO_TOKEN_URL_PARAM:
      "?deviceIdentifier=LENOVO-LenovoA6000-868627023633499&deviceManufacturer=LENOVO&deviceName=Lenovo_A6000&client=androidapp&version=7.1.0&playStore=false&lat=0.0&long=0.0&language=en&networkType=WIFI&imei=868627023633499&osVersion=5.0.2&sim1=89918720400054456304",
    VALIDATE_OTP:
      "login/validate/otp?deviceIdentifier=LENOVO-LenovoA6000-868627023633499&deviceManufacturer=LENOVO&deviceName=Lenovo_A6000&client=androidapp&version=7.1.0&playStore=false&lat=0.0&long=0.0&language=en&networkType=WIFI&imei=868627023633499&osVersion=5.0.2&sim1=89918720400054456304"
  };

otpWrapper.style.display = "none";

resetBtn.addEventListener("click", function () {
  usernameInput.value = "";
  passwordInput.value = "";
});

submitBtn.addEventListener("click", function (event) {
  // console.log("Hello Process", process.env);
  loaderComp.style.display = 'flex';
  getAuthToken();
});

copyToken.addEventListener('click', function () {
  if (tokenInput.value.length > 0) {
    tokenInput.select();
    document.execCommand('copy');
  }
  else {
    alert('Please wait for API resp, or press Login');
  }
})

closeOtp.addEventListener('click', function (event) {
  otpWrapper.style.display = "none";
})

submitOtp.addEventListener("click", function () {
  if (otpInput.value == "") {
    alert("Please enter otp");
    return false;
  }
  submitOtp.setAttribute('disabled', true);
  var dataToSend4 = {
    state: CONFIG.RESP_STATE,
    otp: otpInput.value || 888888
  };
  makeAuthRequest(
    "POST",
    CONFIG.HEROKU_URL + (getEnvtDomain() + CONFIG.VALIDATE_OTP),
    4,
    JSON.stringify(dataToSend4),
    function (response) {
      console.log(response);
      getAccessToken(response);
    }
  );
});

function getEnvtDomain() {
  let domain = document.querySelector('#stgRadio').checked ? 'https://accounts-staging.paytm.in/' : 'https://accounts.paytm.com/';
  return domain;
}

function getAuthToken() {
  if (usernameInput.value == "") {
    alert("Please enter User-ID");
    usernameInput.focus();
    return false;
  } else if (passwordInput.value == "") {
    alert("Please enter password");
    passwordInput.focus();
    return false;
  }
  submitBtn.setAttribute("disabled", true);
  var dataToSend1 =
    "response_type=code&do_not_redirect=true&scope=paytm&client_id=market-app&username=" +
    usernameInput.value +
    "&password=" +
    passwordInput.value;
  makeAuthRequest(
    "POST",
    CONFIG.HEROKU_URL + (getEnvtDomain() + CONFIG.AUTH_STATE_URL),
    1,
    dataToSend1,
    function (response) {
      if (response.statusCode == "01") {
        document.getElementById("otpDisplayMsg").innerHTML =
          response.displayMessage;
        otpWrapper.style.display = "block";
        CONFIG.RESP_STATE = response.state;
      } else if (response.statusCode == "ERROR") {
        alert(response.statusMessage);
        submitBtn.removeAttribute('disabled');
      }
      else {
        getAccessToken(response);
      }
    }
  );
}

function getAccessToken(response) {
  var dataToSend2 =
    "code=" +
    response.code +
    "&scope=paytm&grant_type=authorization_code&client_id=market-app&client_secret=9a071762-a499-4bd9-914a-4361e7c3f4bc";

  makeAuthRequest(
    "POST",
    CONFIG.HEROKU_URL + (getEnvtDomain() + CONFIG.AUTH_ACCESS_TOKEN_URL),
    2,
    dataToSend2,
    function (response) {
      var ssoTokenUrl =
        CONFIG.HEROKU_URL +
        (getEnvtDomain() + CONFIG.AUTH_SSO_TOKEN_URL_HOST) +
        response.access_token +
        CONFIG.AUTH_SSO_TOKEN_URL_PARAM;
      makeAuthRequest("GET", ssoTokenUrl, 3, null, function (response) {
        console.log("Tokens", response.tokens);
        for (var token of response.tokens) {
          if (token.scope === 'wallet') {
            CONFIG.WALLET_TOKEN = token.access_token;
          }
          else if (token.scope === 'paytm') {
            CONFIG.SSO_TOKEN = token.access_token;
          }
        }
        console.log("Logged-In successfully");
        tokenInput.value = CONFIG.SSO_TOKEN;
        console.log(CONFIG.SSO_TOKEN);
        // ssoTokenInput.value = CONFIG.SSO_TOKEN;
        // walletTokenInput.value = CONFIG.WALLET_TOKEN;
        submitBtn.removeAttribute("disabled");
        submitOtp.removeAttribute("disabled");
        otpWrapper.style.display = "none";
        loaderComp.style.display = "none";
        // resetBtn.click();
      });
    }
  );
}

function makeAuthRequest(method, url, requestCode, dataToSend, callback) {
  var xhttp = new XMLHttpRequest();
  xhttp.onreadystatechange = function () {
    if (this.readyState == 4) {
      if (this.status == 200) {
        callback(JSON.parse(this.responseText));
      } else if (this.status >= 400) {
        submitBtn.removeAttribute("disabled");
        submitOtp.removeAttribute("disabled");
        alert("Request Failed- " + JSON.parse(this.responseText).message);
      }
    }
  };
  xhttp.open(method, url, true);
  if (requestCode === 1) {
    xhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
    xhttp.setRequestHeader(
      "Authorization",
      "Basic bWFya2V0LWFwcDo5YTA3MTc2Mi1hNDk5LTRiZDktOTE0YS00MzYxZTdjM2Y0YmM="
    );
  } else if (requestCode === 2) {
    xhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
  } else if (requestCode === 3) {
    xhttp.setRequestHeader(
      "Authorization",
      "Basic bWFya2V0LWFwcDo5YTA3MTc2Mi1hNDk5LTRiZDktOTE0YS00MzYxZTdjM2Y0YmM="
    );
  } else if (requestCode === 4) {
    xhttp.setRequestHeader("Content-Type", "application/json");
  }
  xhttp.send(dataToSend);
}


function setAuthToken() {
  CONFIG.SSO_TOKEN =
    window.AlipayJSBridge.startupParams.SSO_TOKEN || "Default SSO Token";
  ssoTokenInput.value = CONFIG.SSO_TOKEN;
  CONFIG.WALLET_TOKEN =
    window.AlipayJSBridge.startupParams.WALLET_TOKEN || "Default WALLET Token";
  walletTokenInput.value = CONFIG.WALLET_TOKEN;
}

function ready(callback) {
  if (window.AlipayJSBridge) {
    callback && callback();
  } else {
    document.addEventListener("AlipayJSBridgeReady", callback, false);
  }
}

ready(setAuthToken);
